{ name = "my-project"
, dependencies = [ "canvas"
                 , "console"
                 , "effect"
                 , "psci-support"
                 ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
