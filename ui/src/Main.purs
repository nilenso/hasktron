module Main where

import Effect (Effect)
import Effect.Class.Console (log)
import Prelude (Unit)

main :: Effect Unit
main = log "Hello World"
