"use strict";

require("./src/Main.purs").main();

if (module.hot) {
  module.hot.accept();
}

console.log("app starting");
