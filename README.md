# hasktron

## Rules
- The Game runs on a square grid
- The Players (2 or more) can move along horizontal and vertical lines of the grid
- As the players move along the lines they leave behind a Wall
- Players die if they collide with:
     - Boundaries of the grid
     - Their own walls
     - Other players's walls
     - Other players (Both die)
- Player movement:
     - Does not stop (always goes straight)
     - Is at constant speed
	 - Can only turn left or right
- Boost:
     - Can be activated or deactivated by player
	 - Increases speed by a constant factor (boost)
     - Is limited by a maximum "boost fuel"
	 - Fuel decays at a constant rate (when used)
	 - Fuel refills at a constant rate (when not used)
	 - decay rate > refill rate
	 - Deactivated when fuel is exhausted
- Winning a round:
     - Last player standing
	 - Variants
         - Player wall disappears on death
		 - Time spent on the board is score
		 - Points if player dies on your wall
		 - Wall decays at a constant rate
		 - Boost is only available when near a player wall
		 - Pickups -- positive or negative
		 - ...
- User inputs
    - Turn left or right
	- Activate or deactivate boost
	- Self-destruct
