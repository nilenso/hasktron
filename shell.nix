with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "node";
  buildInputs = [
    hlint
    nodejs
    purescript
    purescript-psa
    spago
    stack
  ];
  shellHook = ''
    export PATH="$PWD/ui/node_modules/.bin/:$PATH"
  '';
}
