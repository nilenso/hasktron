module Server where

import Control.Concurrent.MVar
import qualified Data.ByteString as BS
import qualified Network.WebSockets as WS

type Client = (BS.ByteString, WS.Connection)

type ServerState = [Client]

newServerState :: ServerState
newServerState = []

application :: MVar ServerState -> WS.ServerApp
application state pending = do
  conn <- WS.acceptRequest pending
  WS.withPingThread conn 30 (return ()) (return ())
