module Main where

import Control.Concurrent.MVar
import qualified Network.WebSockets as WS

import Server

main :: IO ()
main = do
  state <- newMVar newServerState
  WS.runServer "127.0.0.1" 9160 $ application state
